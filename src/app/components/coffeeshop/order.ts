export class Order {
  public customername: string;
  public email: string;
  public phone: number;
  public drink: string;
  public tempPreference: string;
  public sendText: boolean;

  constructor(name:string, email:string, phone:number, drink:string, temp:string,sendText:boolean) {
    this.customername=name;
    this.email=email;
    this.phone=phone;
    this.drink=drink;
    this.tempPreference=temp;
    this.sendText=sendText;
  }
}
