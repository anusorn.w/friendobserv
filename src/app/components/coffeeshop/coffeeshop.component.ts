import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import  { Order } from './order';

@Component({
  selector: 'app-coffeeshop',
  templateUrl: './coffeeshop.component.html',
  styleUrls: ['./coffeeshop.component.css']
})
export class CoffeeshopComponent implements OnInit {
  shoptitle = "CS Cafe'"

  drinksMenu = ['coffee', 'tea', 'juice'];
  orderModel!: Order;
  orderList: Array<Order> = [];
  orderForm!: FormGroup;

  constructor(private fb:FormBuilder) {
    //this.orderModel = new Order("BOB","bob@jmail.com",0818112318," a","a ", true);
   }

  ngOnInit(): void {
    this.orderForm = this.fb.group ({
      customerName: ['', Validators.required],
      customerEmail: ['', Validators.email],
      customerPhone: ['',[Validators.required, Validators.minLength(8)]],
      customerDrink: ['', Validators.required],
      customerDrinkType: ['', Validators.required],
      custererText: [''],
    });
  }

  confirm_msg = '';
  confirmOrder(data:any): void {
    this.confirm_msg = "Thank you, "
  }
}
