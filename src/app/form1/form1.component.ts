import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Signup } from './signup';

@Component({
  selector: 'app-form1',
  templateUrl: './form1.component.html',
  styleUrls: ['./form1.component.css']
})
export class Form1Component implements OnInit {
  myform: FormGroup;
  who!:Signup;

  constructor(private fb: FormBuilder) {
    this.myform = this.fb.group ({
      phoneNo: [''],
      email: ['', Validators.email],
      password: ['',[Validators.required, Validators.minLength(8)]],
      confirmPassword: ['', Validators.required]
    })
   }

  ngOnInit(): void {
  }

  submitForm() {
    alert(JSON.stringify(this.myform.value));
  }
}
