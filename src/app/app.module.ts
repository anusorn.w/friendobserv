import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { Form1Component } from './form1/form1.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DirectivetestComponent } from './components/directivetest/directivetest.component';
import { AppRoutingModule } from './app-routing.module';
import { CoffeeshopComponent } from './components/coffeeshop/coffeeshop.component';
import { FriendComponent } from './components/friend/friend.component';

import { InMemoryFriendService} from './share/in-memory-friend.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';


@NgModule({
  declarations: [
    AppComponent,
    Form1Component,
    DirectivetestComponent,
    CoffeeshopComponent,
    FriendComponent,
  ],
  imports: [
    BrowserModule, FormsModule, ReactiveFormsModule, AppRoutingModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(InMemoryFriendService, {dataEncapsulation: false})
  ],
  providers: [InMemoryFriendService],
  bootstrap: [AppComponent]
})
export class AppModule { }
