import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { Form1Component } from './form1/form1.component';
import { DirectivetestComponent } from './components/directivetest/directivetest.component';
import { FriendComponent } from './components/friend/friend.component';

const routes: Routes = [
  { path: 'My-Form1', component: Form1Component },
  { path: 'Test-Directive', component: DirectivetestComponent},
  { path: 'Form-and-Data-Model', component: FriendComponent},
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule, RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
